1
00:00:00,300 --> 00:00:03,031
W porządku. Masz naszą uwagę.

2
00:00:03,540 --> 00:00:04,541
Czego chcesz?

3
00:00:04,780 --> 00:00:06,589
Chciałbym porozmawiać z Lois Lane.

4
00:00:06,820 --> 00:00:08,060
Co sprawia, że ​​myślisz, że ona tu jest?

5
00:00:09,020 --> 00:00:11,068
Nie graj ze mną w gry, generale.

6
00:00:11,340 --> 00:00:15,345
Poddam się, ale tylko jeśli ty
zagwarantować wolność Lois.

7
00:00:25,420 --> 00:00:27,388
Dlaczego podporządkowujesz się Zod?

8
00:00:28,580 --> 00:00:32,221
Poddałem się ludzkości.
Jest różnica.

9
00:00:33,340 --> 00:00:35,263
Pozwoliłeś im zakuć w kajdanki?

10
00:00:36,020 --> 00:00:38,421
Nie byłby to zbyt wielki poddaństwo
gdybym się opierał.

11
00:00:39,940 --> 00:00:41,988
A jeśli to sprawia, że ​​czują się bezpieczniej ...

12
00:00:43,060 --> 00:00:44,903
... więc tym lepiej.

13
00:00:49,340 --> 00:00:50,501
Co oznacza skrót "S"?

14
00:00:54,100 --> 00:00:55,704
To nie jest S.

15
00:00:57,180 --> 00:00:58,750
Na moim świecie oznacza to nadzieję.

16
00:01:00,700 --> 00:01:04,227
Cóż, tutaj jest to S.

17
00:01:06,260 --> 00:01:07,785
Co powiesz na...

18
00:01:12,140 --> 00:01:13,141
...Wspaniały...

19
00:01:13,380 --> 00:01:14,427
Pan?

20
00:01:14,660 --> 00:01:18,028
- Cześć, mam na imię Dr. E ...
- Emil Hamilton.

21
00:01:18,260 --> 00:01:21,230
Wiem, widzę twój tag ID
w kieszeni na piersi.

22
00:01:21,460 --> 00:01:23,542
Wraz z na wpół zjedzoną rolą Lifesavers.

23
00:01:24,700 --> 00:01:27,021
Mogę też zobaczyć żołnierzy
w następnym pokoju...

24
00:01:27,260 --> 00:01:29,581
... przygotowanie tego środka uspokajającego
Twoje.

25
00:01:29,820 --> 00:01:31,265
Nie będziesz tego potrzebować.

26
00:01:31,540 --> 00:01:34,589
Sir, nie możesz nas oczekiwać
nie podejmować środków ostrożności.

27
00:01:34,820 --> 00:01:37,266
Mógłbyś nieść
jakiś rodzaj obcego patogenu.

28
00:01:37,500 --> 00:01:38,945
Byłem tu przez 33 lata, doktorze.

29
00:01:39,220 --> 00:01:43,145
- Jeszcze nikogo nie zainfekowałem.
- To wiesz. Mamy uzasadnione ...

30
00:01:43,380 --> 00:01:46,748
... obawy dotyczące bezpieczeństwa. Odkryłeś
twoją tożsamość z panną Lane.

31
00:01:47,740 --> 00:01:49,981
Dlaczego nie zrobisz tego samego z nami?

32
00:01:50,940 --> 00:01:53,261
Połóżmy tutaj nasze karty,
generał.

33
00:01:54,700 --> 00:01:56,429
Boisz się, bo nie możesz mnie kontrolować.

34
00:01:57,020 --> 00:01:59,546
Ty nie, a ty nigdy tego nie zrobisz.

35
00:02:00,380 --> 00:02:02,348
Ale to nie znaczy, że jestem twoim wrogiem.

36
00:02:02,620 --> 00:02:04,110
Więc kim jest?

37
00:02:04,340 --> 00:02:05,785
Zod?

38
00:02:06,380 --> 00:02:07,825
Właśnie o to się martwię.

39
00:02:08,380 --> 00:02:10,109
Bądź tak jak może ...

40
00:02:10,340 --> 00:02:13,150
... Dostałem rozkazy
przekazać ci go.

41
00:02:14,380 --> 00:02:16,269
Rób to, co musisz zrobić, generale.

42
00:02:21,020 --> 00:02:21,828
Dziękuję Ci.

43
00:02:22,940 --> 00:02:23,907
Po co?

44
00:02:25,100 --> 00:02:26,670
Za wiarę we mnie.

45
00:02:29,700 --> 00:02:31,748
Na końcu nie robiło to wielkiej różnicy.

46
00:02:32,580 --> 00:02:34,150
To mi się udało.

47
00:02:54,740 --> 00:02:56,310
Nadchodzą.

48
00:02:57,060 --> 00:02:58,585
Powinieneś teraz wyjść.

49
00:03:01,700 --> 00:03:03,270
Idź, Lois.

50
00:04:14,140 --> 00:04:15,551
Kal-EI.

51
00:04:16,060 --> 00:04:18,108
Jestem podrzędnym dowódcą Faora-UI.

52
00:04:18,779 --> 00:04:22,022
W imieniu generała Zoda,
Pozdrawiam Cię jego pozdrowieniami.

53
00:04:28,100 --> 00:04:31,183
- Czy jesteś tu w rankingu?
- Jestem.

54
00:04:31,420 --> 00:04:33,388
Generał Zod chciałby tę kobietę ...

55
00:04:33,620 --> 00:04:35,270
... aby mi towarzyszyć.

56
00:04:36,220 --> 00:04:37,346
Poprosiłeś o kosmitę.

57
00:04:38,780 --> 00:04:41,704
Nic nie powiedziałeś
o jednym z naszych.

58
00:04:41,980 --> 00:04:45,109
Czy mam powiedzieć generałowi?
nie chcesz się podporządkować?

59
00:04:45,340 --> 00:04:47,229
Nie obchodzi mnie, co mu mówisz.

60
00:04:50,220 --> 00:04:51,665
W porządku.

61
00:04:52,740 --> 00:00:00,000
Pójdę.

