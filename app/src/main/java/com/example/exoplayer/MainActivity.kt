package com.example.exoplayer

import android.app.AlertDialog
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MergingMediaSource
import com.google.android.exoplayer2.source.SingleSampleMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.DefaultTrackNameProvider
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.MimeTypes
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var player: SimpleExoPlayer
    private lateinit var trackSelector: DefaultTrackSelector

    private var selectedTrack = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        trackSelector = DefaultTrackSelector()
        player = ExoPlayerFactory.newSimpleInstance(this, DefaultRenderersFactory(this), trackSelector)
        player.playWhenReady = true
        exoPlayer.player = player
        exoPlayer.subtitleView.visibility = View.VISIBLE
        exoPlayer.keepScreenOn = true

        val dataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "ZR-ExoPlayer"))
        val videoSuperman = ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse("asset:///superman.mp4"))
        val videoYufid = ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse("asset:///yufid.mp4"))
        val subEnFormat = Format.createTextSampleFormat(null, MimeTypes.TEXT_VTT, Format.NO_VALUE, "en")
        val subEnSource = SingleSampleMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse("asset:///english.vtt"), subEnFormat, C.TIME_UNSET)
        val subIndFormat = Format.createTextSampleFormat(null, MimeTypes.TEXT_VTT, Format.NO_VALUE, "ind")
        val subIndSource = SingleSampleMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse("asset:///indonesian.vtt"), subIndFormat, C.TIME_UNSET)
        val subKoreanFormat = Format.createTextSampleFormat(null, MimeTypes.APPLICATION_SUBRIP, Format.NO_VALUE, Locale.KOREAN.language)
        val subKoreanSource = SingleSampleMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse("asset:///korean.srt"), subKoreanFormat, C.TIME_UNSET)
        val subPolishFormat = Format.createTextSampleFormat(null, MimeTypes.APPLICATION_SUBRIP, Format.NO_VALUE, "pl")
        val subPolishSource = SingleSampleMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse("asset:///polish.srt"), subPolishFormat, C.TIME_UNSET)
        val mergeSource = MergingMediaSource(videoSuperman, subEnSource, subIndSource, subKoreanSource, subPolishSource)
        val concenate = ConcatenatingMediaSource(mergeSource, videoYufid)
        player.prepare(concenate)

        player.addListener(object:Player.EventListener{
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playbackState == Player.STATE_READY) {
                    debug()
                }
            }
        })

        btGantiSub.setOnClickListener {
            showDialogLanguage()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        player.release()
    }

    private fun showDialogLanguage() {
        var subtitleRenderer = -1
        val mapInfo = trackSelector.currentMappedTrackInfo!!
        val trackNameProvider = DefaultTrackNameProvider(resources)
        val subList = arrayListOf<Triple<String, Int, Int>>()

        for (rendererIndex in 0 until mapInfo.rendererCount) {
            val type = mapInfo.getRendererType(rendererIndex)
            if (type == C.TRACK_TYPE_TEXT) {
                subtitleRenderer = rendererIndex
                val trackGroups = mapInfo.getTrackGroups(rendererIndex)
                for (groupIndex in 0 until trackGroups.length) {
                    val group = trackGroups[groupIndex]
                    for (trackIndex in 0 until group.length) {
                        val trackName = trackNameProvider.getTrackName(group.getFormat(trackIndex))
                        subList.add(Triple(trackName, groupIndex, trackIndex))
                    }
                }
                break
            }
        }

        val view = LinearLayout(this).apply { orientation = LinearLayout.VERTICAL }
        val dialog = AlertDialog.Builder(this)
            .setView(view)
            .show()

        subList.forEach {sub ->
            view.addView(TextView(this).apply {
                text = sub.first
                textSize = 18F
                setPadding(20, 20, 20, 20)
                setOnClickListener {
                    val override = DefaultTrackSelector.SelectionOverride(sub.second, sub.third)
                    val param = trackSelector.buildUponParameters()
                    param.setSelectionOverride(subtitleRenderer, mapInfo.getTrackGroups(subtitleRenderer), override)
                    trackSelector.setParameters(param)
                    dialog.dismiss()
                }
            })
        }
    }

    private fun debug() {
        val trackNameProvider = DefaultTrackNameProvider(resources)
        val mapInfo = trackSelector.currentMappedTrackInfo!!
        for (rendererIndex in 0 until mapInfo.rendererCount) {
            val type = mapInfo.getRendererType(rendererIndex)
            when (type) {
                C.TRACK_TYPE_TEXT -> Log.i("AOEU", "AA$rendererIndex TEXT")
                C.TRACK_TYPE_VIDEO -> Log.i("AOEU", "AA$rendererIndex VIDEO")
                C.TRACK_TYPE_AUDIO -> Log.i("AOEU", "AA$rendererIndex AUDIO")
                else -> Log.i("AOEU", "Kocak loh")
            }

            val trackGroups = mapInfo.getTrackGroups(rendererIndex)
            for (trackGroupsIndex in 0 until trackGroups.length) {
                val group = trackGroups[trackGroupsIndex]
                Log.i("AOEU", "    TrackGroup$trackGroupsIndex")
                for (groupIndex in 0 until group.length) {
                    val trackName = trackNameProvider.getTrackName(group.getFormat(groupIndex))
                    Log.i("AOEU", "        BB$groupIndex $trackName")
                }
            }
        }
    }
}
